<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APITest extends TestCase
{
    private $api;

    protected function setUp(): void
    {
        $this->api = new API();
    }

    // Testcase 1
    // Assume exact data is in the database
    //      'id' => 1
    //      'first_name' => 'John'
    //      'middle_name' => ''
    //      'last_name' => 'Doe'
    //      'contact_number' => 123
    public function testHttpGetWithValidIdPayload()
    {
        $payload = ['id' => 1];
        $expectedRow = ['id' => 1, 'first_name' => 'John', 'middle_name' => '', 'last_name' => 'Doe', 'contact_number' => 123];
        $expectedResponse = [
            'status' => 'success',
            'message' => 'Query executed successfully.',
            'data' => $expectedRow
        ];

        $actualResponse = $this->api->httpGet($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    // Testcase 4
    // Expected to return a list
    // Assume only these data is in the database:
    // ['id' => 1, 'first_name' => 'John', 'middle_name' => '', 'last_name' => 'Doe', 'contact_number' => 123], 
    // ['id' => 2, 'first_name' => 'Doe', 'middle_name' => '', 'last_name' => 'John', 'contact_number' => 321]
    public function testHttpGetWithValidEmptyPayload()
    {
        $payload = [];
        $expectedRow = array(['id' => 1, 'first_name' => 'John', 'middle_name' => '', 'last_name' => 'Doe', 'contact_number' => 123], 
        ['id' => 2, 'first_name' => 'Doe', 'middle_name' => '', 'last_name' => 'John', 'contact_number' => 321]);
        $expectedResponse = [
            'status' => 'success',
            'message' => 'Query executed successfully.',
            'data' => $expectedRow
        ];

        $actualResponse = $this->api->httpGet($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    // Testcase 5
    public function testHttpGetWithInvalidPayload()
    {
        $payload = 'invalid_payload'; 
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be an array.'
        ];

        $actualResponse = $this->api->httpGet($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPostWithValidPayload()
    {
        $payload = ['first_name' => 'John', 'middle_name' => '', 'last_name' => 'Doe', 'contact_number' => 123];
        $expectedResponse = [
            'status' => 'success',
            'message' => 'Query executed successfully.',
            'data' => 'Data inserted successfully.'
        ];

        $actualResponse = $this->api->httpPost($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPostWithInvalidPayload()
    {
        $payload = 'Invalid payload';
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = $this->api->httpPost($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPostWithInvalidEmptyPayload()
    {
        $payload = [];
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = $this->api->httpPost($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    // Updating the first_name of userId 1 from 'John' to 'Johnny
    public function testHttpPutValidPayload()
    {
        $payload = ['id' => 1, 'first_name' => 'Johnny', 'middle_name' => '', 'last_name' => 'Doe', 'contact_number' => 123];
        $expectedResponse = [
            'status' => 'success',
            'message' => 'Query executed successfully.',
            'data' => 'Data updated successfully.'
        ];

        $actualResponse = $this->api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }
    
    public function testHttpPutInvalidPayloadWhereIDDoesNotExist()
    {
        $payload = ['first_name'=> 'NO ID'];
        $expectedResponse = [
            'status' => 'error',
            'message' => 'ID does not exist in the table.'
        ];

        $actualResponse = $this->api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPutInvalidNoIDInPayload()
    {
        $payload = ['first_name'=> 'NO ID'];
        $expectedResponse = [
            'status' => 'error',
            'message' => 'There must be an ID in the request data.'
        ];

        $actualResponse = $this->api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPutWithInvalidPayload()
    {
        $payload = 'Invalid Payload.';
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = $this->api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPutWithInvalidEmptyPayload()
    {
        $payload = [];
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = $this->api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpDeleteWithValidId()
    {
        $id = 1;
        $expectedResponse = [
            'status' => 'success',
            'message' => 'Query executed successfully.',
            'data' => 'Data deleted successfully.'
        ];

        $actualResponse = $this->api->httpDelete($id);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpDeleteWithInvalidId()
    {
        $id = 'invalid_id';
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid ID. ID must be a non-empty numeric value.'
        ];

        $actualResponse = $this->api->httpDelete($id);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    // public function testHttpPost()
    // {
    //     $_SERVER['REQUEST_METHOD'] = 'POST';


    //     $payload = array(
    //         'first_name' => 'Test',
    //         'middle_name' => 'test',
    //         'last_name' => 'last test',
    //         'contact_number' => 654655
    //     );
    //     $result = json_decode($this->api->httpPost($payload), true);
    //     $this->assertArrayHasKey('status', $result);
    //     $this->assertEquals($result['status'], 'success');
    //     $this->assertArrayHasKey('data', $result);
    // }
}

?>