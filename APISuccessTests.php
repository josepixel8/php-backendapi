<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APISuccessTests extends TestCase
{
    private static $api;

    public static function setUpBeforeClass(): void
    {
        $hostname = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'employee';
        $port = 3307;

        // Establish a database connection
        $db = new MysqliDb($hostname, $username, $password, $database, $port, 'utf8', 'my_');

        self::$api = new API($db);

        // FOR LOCAL TESTING ONLY
        // Execute SQL statements
        self::$api->executeSQLStatements([
            'DELETE FROM information',
            'ALTER TABLE information AUTO_INCREMENT = 1',
        ]);
    }

    /**
     * Data provider method
     */
    public function testHttpPayload()
    {
        $payload = array(
            [
                'id' => 1, 
                'first_name' => "John", 
                'middle_name' => "Knee", 
                'last_name' => "Doe",
                'contact_number' => 123
                ],
                [
                    'id' => 2, 
                    'first_name' => "Doe", 
                    'middle_name' => "Knee", 
                    'last_name' => "John",
                    'contact_number' => 321
                ],
        );

        $load = $payload;
        $this->assertNotEmpty($load);

        return $load;
    }

    /**
     * @depends testHttpPayload 
     */
    public function testHttpPostWithValidPayload($data)
    {        
        foreach($data as $payload){
            $expectedResponse = [
                'status' => 'success',
                'message' => 'Query executed successfully.',
                'data' => $payload
            ];
    
            $actualResponse = self::$api->httpPost($payload);
    
    
            $this->assertNotEmpty($payload);
            $this->assertEquals($expectedResponse, $actualResponse);    
        }

        return $data;
    }

    /**
     * @depends testHttpPostWithValidPayload
     * 
     */
    public function testHttpGetWithValidIdPayload($data)
    {
        foreach($data as $payload){
            $expectedRow['id'] = $payload['id'];
            $expectedRow['first_name'] = $payload['first_name'];
            $expectedRow['middle_name'] = $payload['middle_name'];
            $expectedRow['last_name'] = $payload['last_name'];
            $expectedRow['contact_number'] = $payload['contact_number'];
            
            $expectedResponse = [
                'status' => 'success',
                'message' => 'Query executed successfully.',
                'data' => $expectedRow
            ];

            $actualResponse = self::$api->httpGet($payload);

            $this->assertEquals($expectedResponse, $actualResponse);
        }

        return $data;
    }

    /**
     * @depends testHttpGetWithValidIdPayload
     */
    public function testHttpGetWithValidEmptyPayload($expectedData)
    {
        $payload = [];
        $expectedRow = $expectedData;

        $expectedResponse = [
            'status' => 'success',
            'message' => 'Query executed successfully.',
            'data' => $expectedRow
        ];

        $actualResponse = self::$api->httpGet($payload);

        $this->assertEquals($expectedResponse, $actualResponse);

        return $expectedData;
    }

    /**
     * @depends testHttpGetWithValidEmptyPayload
     */
    public function testHttpPutValidPayload($data)
    {
        foreach($data as $payload){
            $payload['first_name'] = 'Johnny'; // update first_name to 'Johnny'

            $expectedResponse = [
                'status' => 'success',
                'message' => 'Query executed successfully.',
                'data' => $payload
            ];

            $actualResponse = self::$api->httpPut($payload);

            $this->assertEquals($expectedResponse, $actualResponse);
        }

        return $data;
    }

    /**
     * @depends testHttpPutValidPayload
     */
    public function testHttpDeleteWithValidId($data)
    {
        foreach($data as $payload){
            $id = $payload['id'];

            $expectedResponse = [
                'status' => 'success',
                'message' => 'Query executed successfully.',
                'data' => ''
            ];
    
            $actualResponse = self::$api->httpDelete($id);
    
            $this->assertEquals($expectedResponse, $actualResponse);
        }
    }
}

?>