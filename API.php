<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');

class API {
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }

    function httpGet($payload) {
        if (!is_array($payload)) {
            return $this->errorResponse('Invalid payload. Payload must be an array.');
        }
        
        if(array_key_exists('id', $payload)){
            // TODO: check if id is a number & exists

            // Get by ID
            $this->db->where('id', $payload['id']);
            $row =  $this->db->getOne('information', '*');

            if($row){
                // Proceed to getting information of employee
                $this->db->where('id', $payload['id']);

                // Get only 1 employee from table
                $result = $this->db->getOne('information');
            }else{
                // ID does not exist
                echo ("ID does not exist in the database.");
                
                // Query failed, return failed response
                return $this->errorResponse('Failed Fetch Request');
            }
        }else{
            // Get only 1 employee from table
            $result = $this->db->get('information', null, '*');
        }
        
        if ($result !== false) {
            // Query executed successfully, return success response
            return $this->successResponse($result);
        }

        // Query failed, return failed response
        return $this->errorResponse('Failed Fetch Request');
    }
     
        
    function successResponse($data){
        return [
            'status' => 'success',
            'message' => 'Query executed successfully.',
            'data' => $data
        ];
    }
        
    function errorResponse($errorMessage){
        return [
            'status' => 'error',
            'message' => $errorMessage
        ];
    }
        
    function httpPost($payload) {
        if (!is_array($payload) || empty($payload)) {
            return $this->errorResponse('Invalid payload. Payload must be a non-empty array.');
        } 

        if(isset($payload['first_name']) && isset($payload['middle_name']) && isset($payload['last_name']) && isset($payload['contact_number']) && (empty($payload['first_name']) || empty($payload['middle_name']) || empty($payload['last_name']) || empty($payload['contact_number']))){
            return $this->errorResponse('Required fields are missing.');
        }

        // Perform the database insertion logic here
        $result = $this->db->insert('information', $payload);
            
        if ($result !== false) {
            // Query executed successfully, return success response
            return $this->successResponse($payload);
        } 
        
        // Query failed, return failed response
        return $this->errorResponse('Failed to Insert Data');
    }
        
    function httpPut($payload) {
        if (!is_array($payload) || empty($payload)) {
            return $this->errorResponse('Invalid payload. Payload must be a non-empty array.');
        } 

        if(isset($payload['id']) && isset($payload['first_name']) && isset($payload['middle_name']) && isset($payload['last_name']) && isset($payload['contact_number']) && !empty($payload['id']) && !empty($payload['first_name']) && !empty($payload['middle_name']) && !empty($payload['last_name']) && !empty($payload['contact_number'])){
            $id = $payload['id'];

            if (!is_numeric($id)) {
                return $this->errorResponse('Invalid ID. ID must be a numeric value.');
            }else{
                // Perform the database update logic here
                $this->db->where('id', $id);

                // Check if any row matches the condition
                $exists = $this->db->has('information');

                if ($exists) {
                    // 'id' exists in the table
                    $this->db->where('id', $id);
                    $result = $this->db->update('information', $payload);

                    if ($result !== false) {
                        // Query executed successfully, return success response
                        return $this->successResponse($payload);
                    }
                } else {
                    // 'id' does not exist in the table
                    return $this->errorResponse('ID does not exist in the database.');
                }
            }
        }else{
            return $this->errorResponse('Required fields are missing.');
        }

        // Query failed, return failed response
        return $this->errorResponse('Failed to Update Data');
    }
        
    function httpDelete($id) {
        if (empty($id) || !is_numeric($id)) {
            return $this->errorResponse('Invalid ID. ID must be a non-empty numeric value.');
        }
        
        $this->db->where('id', $id);

        // Check if any row matches the condition
        // $exists = $this->db->has('information');
        $row =  $this->db->getOne('information');
        if ($row) {
            // 'id' exists in the table
            $this->db->where('id', $id);
            $result = $this->db->delete('information');
            if ($result !== false) {
                // Query executed successfully, return success response
                return $this->successResponse('');
            } 
        } else {
            // 'id' does not exist in the table
            return $this->errorResponse("ID does not exist in the database.");
        }
        
        // Query failed, return failed response
        return $this->errorResponse('Failed to Delete Data');
    }

    function executeSQLStatements(array $sqlStatements): void
    {
        // Example using mysqli extension
        $mysqli = new mysqli('localhost', 'root', '', 'employee' , 3307);

        if ($mysqli->connect_error) {
            die('Connection failed: ' . $mysqli->connect_error);
        }

        foreach ($sqlStatements as $sql) {
            $mysqli->query($sql);
        }

        $mysqli->close();
    }
}

// Identifier if what type of request
// $request_method = $_SERVER['REQUEST_METHOD'];
$request_method = (array_key_exists('REQUEST_METHOD', $_SERVER))? $_SERVER['REQUEST_METHOD']: '';

// For GET, POST, PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    // Check if method is PUT or DELETE and get the ids from the URL
    if ($request_method === 'DELETE') {

        // Get id from request_uri
        // example: localhost/backend-api/API.php?id=1
        //      $ids = 1
        $id = $_GET['id'];
         
        // Additional code for processing the extracted ids if needed
    }
 
    // Payload data
    $received_data = json_decode(file_get_contents('php://input'), true);
}

$hostname = 'localhost';
$username = 'root';
$password = '';
$database = 'employee';
$port = 3307;

// Establish a database connection
$db = new MysqliDb($hostname, $username, $password, $database, $port, 'utf8', 'my_');
$api = new API($db);

// Check if connected to the database
$conn = new mysqli($hostname, $username, $password, $database, $port);
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

echo '<br>Connected successfully<br>';

$response = "";

//Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $response = $api->httpGet($received_data);
        break;
    case 'POST':
        $response = $api->httpPost($received_data);
        break;
    case 'PUT':
        $response = $api->httpPut($received_data);
        break;
    case 'DELETE':
        $response = $api->httpDelete($id);
        break;
}

echo json_encode($response);
?> 