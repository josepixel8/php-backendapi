<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APIFailedTests extends TestCase
{
    private static $api;

    public static function setUpBeforeClass(): void
    {
        $hostname = 'localhost';
        $username = 'root';
        $password = '';
        $database = 'employee';
        $port = 3307;

        // Establish a database connection
        $db = new MysqliDb($hostname, $username, $password, $database, $port, 'utf8', 'my_');

        self::$api = new API($db);

        // FOR LOCAL TESTING ONLY
        // Execute SQL statements
        self::$api->executeSQLStatements([
            'DELETE FROM information',
            'ALTER TABLE information AUTO_INCREMENT = 1',
        ]);
    }

    public function testHttpPostWithInvalidPayload()
    {
        $payload = 'Invalid payload';
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = self::$api->httpPost($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPostWithInvalidEmptyPayload()
    {
        $payload = [];
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = self::$api->httpPost($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPostWithEmptyField()
    {
        $payload = json_decode('{"first_name": "John", "middle_name": "Knee", "last_name": "", "contact_number": 123}', true);
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Required fields are missing.'
        ];

        $actualResponse = self::$api->httpPost($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpGetWithInvalidPayload()
    {
        $payload = 'invalid_payload'; 
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be an array.'
        ];

        $actualResponse = self::$api->httpGet($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }
    
    public function testHttpPutInvalidPayloadWhereIDDoesNotExist()
    {
        $payload = json_decode('{"id": 1433425, "first_name": "Johnny", "middle_name": "Knee", "last_name": "Doe", "contact_number": 123}', true);
        
        $expectedResponse = [
            'status' => 'error',
            'message' => 'ID does not exist in the database.'
        ];

        $actualResponse = self::$api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPutInvalidNoIDInPayload()
    {
        $payload = json_decode('{"first_name": "NO ID IN PAYLOAD"}', true);
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Required fields are missing.'
        ];

        $actualResponse = self::$api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPutWithInvalidPayload()
    {
        $payload = 'Invalid Payload.';
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = self::$api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpPutWithInvalidEmptyPayload()
    {
        $payload = [];
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid payload. Payload must be a non-empty array.'
        ];

        $actualResponse = self::$api->httpPut($payload);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpDeleteWithInvalidId()
    {
        $id = 'invalid_id';
        $expectedResponse = [
            'status' => 'error',
            'message' => 'Invalid ID. ID must be a non-empty numeric value.'
        ];

        $actualResponse = self::$api->httpDelete($id);

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    public function testHttpDeleteWithIdNotExist()
    {
        $id = 1345435;
        $expectedResponse = [
            'status' => 'error',
            'message' => 'ID does not exist in the database.'
        ];

        $actualResponse = self::$api->httpDelete($id);

        $this->assertEquals($expectedResponse, $actualResponse);
    }
}

?>